'use strict';

const gulp = require('gulp'), //основной плагин gulp
    sass = require('gulp-sass'), //препроцессор stylus
    prefixer = require('gulp-autoprefixer'), //расставление автопрефиксов
    cleanCSS = require('gulp-clean-css'), //минификация css
    uglify = require('gulp-uglify'), //минификация js
    sourcemaps = require('gulp-sourcemaps'), //sourcemaps
    rigger = require('gulp-rigger'), //работа с инклюдами в html и js
    rename = require("gulp-rename"), //переименвоание файлов
    watch = require('gulp-watch'), //расширение возможностей watch
    browserSync = require('browser-sync').create();



const path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'build/',
        jsOwn: 'build/js/custom/',
        jsVendor: 'build/js/vendor/',
        css: 'build/css/',
        imgCSS: 'build/css/images/',
        fonts: 'build/fonts/',
        htaccess: 'build/',
        contentImg: 'build/img/',
    },
    src: { //Пути откуда брать исходники
        html: 'src/template/**/*.html', //Синтаксис src/template/*.html говорит gulp что мы хотим взять все файлы с расширением .html
        jsOwn: 'src/js/custom/**/[^_]*.js',//В стилях и скриптах нам понадобятся только main файлы\
        jsVendor: 'src/js/vendor/**/[^_]*.js',
        jshint: 'src/js/*.js',
        css: 'src/css/styles.scss',
        cssVendor: 'src/css/vendor/**/*.*', //Если мы хотим файлы библиотек отдельно хранить то раскоментить строчку
        imgCSS: 'src/css/images/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
        fonts: 'src/fonts/**/*.*',
        contentImg: 'src/img/**/*.*',
        htaccess: 'src/.htaccess'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        html: 'src/template/*.html',
        js: 'src/js/**/*.js',
        css: 'src/css/**/*.*',
        imgCSS: 'src/css/images/**/*.*',
        contentImg: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*',
        htaccess: 'src/.htaccess',
        sprites: 'src/css/sprites/*.png'
    },
    clean: './build', //директории которые могут очищаться
    outputDir: './build' //исходная корневая директория для запуска минисервера
};

// Локальный сервер для разработки
function connect() {
    return browserSync.init({
        server: {
            baseDir: path.outputDir
        }
    });
}

function html() {
    return gulp.src(path.src.html) //Выберем файлы по нужному пути
        .pipe(rigger()) //Прогоним через rigger
        .pipe(gulp.dest(path.build.html)) //выгрузим их в папку build
        .pipe(browserSync.stream()) //И перезагрузим наш сервер для обновлений
}

// билдинг яваскрипта
function jsOwn() {
    return gulp.src(path.src.jsOwn) //Найдем наш main файл
        .pipe(rigger()) //Прогоним через rigger
        .pipe(sourcemaps.init()) //Инициализируем sourcemap
        .pipe(uglify()) //Сожмем наш js
        //.pipe(sourcemaps.write()) //Пропишем карты
        .pipe(rename({suffix: '.min'})) //добавим суффикс .min к выходному файлу
        .pipe(gulp.dest(path.build.jsOwn)) //выгрузим готовый файл в build
        .pipe(browserSync.stream()) //И перезагрузим сервер
}

function jsVendor() {
    return gulp.src(path.src.jsVendor) //Найдем наш main файл
        .pipe(rigger()) //Прогоним через rigger
        .pipe(uglify())
        .pipe(rename({suffix: '.min'})) //добавим суффикс .min к выходному файлу
        .pipe(gulp.dest(path.build.jsVendor)) //выгрузим готовый файл в build
        .pipe(browserSync.stream()) //И перезагрузим сервер
}

// билдинг пользовательского css
function cssOwn() {
    return gulp.src(path.src.css) //Выберем наш основной файл стилей
        .pipe(sourcemaps.init()) //инициализируем soucemap
        .pipe(sass()) //Скомпилируем SaSS
        .pipe(prefixer({
            browsers: ['last 3 version', "> 1%", "ie 8", "ie 7"]
        })) //Добавим вендорные префиксы
        .pipe(cleanCSS({compatibility: 'ie9', rebase: false})) //Сожмем
        //.pipe(sourcemaps.write()) //пропишем sourcemap
        .pipe(rename({suffix: '.min'})) //добавим суффикс .min к имени выходного файла
        .pipe(gulp.dest(path.build.css)) //вызгрузим в build
        .pipe(browserSync.stream()) //перезагрузим сервер
}

// билдинг вендорного css
function cssVendor() {
    return gulp.src(path.src.cssVendor) // Берем папку vendor
        .pipe(sourcemaps.init()) //инициализируем soucemap
        .pipe(sass()) //Скомпилируем SaSS
        .pipe(prefixer({
            browsers: ['last 3 version', "> 1%", "ie 8", "ie 7"]
        })) //Добавим вендорные префиксы
        .pipe(cleanCSS({compatibility: 'ie9', rebase: false})) //Сожмем
        //.pipe(sourcemaps.write()) //пропишем sourcemap
        .pipe(rename({suffix: '.min'})) //добавим суффикс .min к имени выходного файла
        .pipe(gulp.dest(path.build.css)) //вызгрузим в build
        .pipe(browserSync.stream()) //перезагрузим сервер
}

// билдим шрифты
function fonts() {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts)) //выгружаем в build
}

// билдим htaccess
function htaccess() {
    return gulp.src(path.src.htaccess)
        .pipe(gulp.dest(path.build.htaccess)) //выгружаем в build
}

// билдим статичные изображения
function imageCSS() {
    return gulp.src(path.src.imgCSS) //Выберем наши картинки
    /* .pipe(imagemin({ //Сожмем их
         progressive: true, //сжатие .jpg
         svgoPlugins: [{removeViewBox: false}], //сжатие .svg
         interlaced: true, //сжатие .gif
         optimizationLevel: 3 //степень сжатия от 0 до 7
     }))*/
        .pipe(gulp.dest(path.build.imgCSS)) //выгрузим в build
        .pipe(browserSync.stream()) //перезагрузим сервер
}

// билдим динамичные изображения
function imagesContent() {
    return gulp.src(path.src.contentImg)
    /*.pipe(imagemin({ //Сожмем их
        progressive: true, //сжатие .jpg
        svgoPlugins: [{removeViewBox: false}], //сжатие .svg
        interlaced: true, //сжатие .gif
        optimizationLevel: 3 //степень сжатия от 0 до 7
    }))*/
        .pipe(gulp.dest(path.build.contentImg)) //выгрузим в build
        .pipe(browserSync.stream()) //перезагрузим сервер
}

// билдим css целиком
gulp.task('image_build', gulp.series(imageCSS, imagesContent));
gulp.task('css_build', gulp.series(cssOwn, cssVendor));
gulp.task('js_build', gulp.series(jsOwn, jsVendor));

// билдим все
gulp.task('build', gulp.parallel(
    html,
    'js_build',
    'css_build',
    fonts,
    htaccess,
    'image_build'
));

function clean() {
    return del(path.clean);
}

gulp.task('clean', clean);

// watch
gulp.task('watch', function () {
    //билдим html в случае изменения
    watch(path.watch.html, html);

    //билдим контекстные изрображения в случае изменения
    watch(path.watch.contentImg, imagesContent);
    //билдим css в случае изменения
    watch(path.watch.css, gulp.series('css_build'));

    //билдим js в случае изменения
    watch(path.watch.js, gulp.series('js_build'));

    //билдим статичные изображения в случае изменения
    watch(path.watch.imgCSS, imageCSS);

    //билдим шрифты в случае изменения
    watch(path.watch.fonts, fonts);

    //билдим htaccess в случае изменения
    watch(path.watch.htaccess, htaccess);
});

gulp.task('default', gulp.parallel('build', 'watch', connect));